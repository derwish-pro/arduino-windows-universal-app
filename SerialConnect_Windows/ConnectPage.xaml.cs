﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConnectPage : Page
    {
        public ConnectPage()
        {
            this.InitializeComponent();

            GetSerialList();

        }

        private DeviceInformationCollection devices;

        public async void GetSerialList()
        {


            string selector = SerialDevice.GetDeviceSelector();
            devices = await DeviceInformation.FindAllAsync(selector);


            foreach (var device in devices)
                listbox1.Items.Add(device.Name);
            
        }



        private async void buttonConnect_Click(object sender, RoutedEventArgs e)
        {
            
              int selection = listbox1.SelectedIndex;
              DeviceInformation device = devices[selection];

              App.serialPort = await SerialDevice.FromIdAsync(device.Id);
              App.serialPort.WriteTimeout = TimeSpan.FromMilliseconds(1000);
              App.serialPort.ReadTimeout = TimeSpan.FromMilliseconds(1000);
              App.serialPort.BaudRate = 9600;
              App.serialPort.Parity = SerialParity.None;
              App.serialPort.StopBits = SerialStopBitCount.One;
              App.serialPort.DataBits = 8;

             Frame.Navigate(typeof(ControlPage));
        }

     

    }
}
