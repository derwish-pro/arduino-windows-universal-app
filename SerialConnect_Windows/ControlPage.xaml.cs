﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ControlPage : Page
    {
        DataReader dataReaderObject;

        public ControlPage()
        {
            this.InitializeComponent();

            StartReading();

        }




        private async void toggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            if (toggleSwitch.IsOn)
                await WriteAsync("ledon 13\r\n");
            else
                await WriteAsync("ledoff 13\r\n");
        }

        private async Task WriteAsync(string message)
        {
            DataWriter dataWriteObject = new DataWriter(App.serialPort.OutputStream);

            dataWriteObject.WriteString(message);

            await dataWriteObject.StoreAsync();

            dataWriteObject.DetachStream();
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            await WriteAsync("temp\r\n");
        }



        private async void StartReading()
        {

            dataReaderObject = new DataReader(App.serialPort.InputStream);

            while (true)
                await ReadAsync();

        }

        private async Task ReadAsync()
        {
            uint bytesRead = await dataReaderObject.LoadAsync(1024);

            textBox.Text = dataReaderObject.ReadString(bytesRead);
        }
    }
}
