	#include <SerialCommand.h> 

	SerialCommand     serialCommand;


	void setup() {
		Serial.begin(9600);
		serialCommand.addCommand("ledon", SetLedOn);
		serialCommand.addCommand("ledoff", SetLedOff);
		serialCommand.addCommand("temp", GetTemp);
	}


	void loop() {
		serialCommand.readSerial();
	}


	void SetLedOn() {
		char *arg = serialCommand.next();
		int pin = atoi(arg);

		pinMode(pin, OUTPUT);
		digitalWrite(pin, HIGH);

	}


	void SetLedOff() {
		char *arg = serialCommand.next();
		int pin = atoi(arg);

		pinMode(pin, OUTPUT);
		digitalWrite(pin, LOW);

	}


	void GetTemp() {
		float temperature = -9.99;
		Serial.println(temperature, 2);
	}





/*

#include <SerialCommand.h>      // Steven Cogswell ArduinoSerialCommand library from http://GitHub.com

#define LED_LOW      3
#define LED_HIGH     13
#define DBGMSG(A)    if (dbg){ Serial.print("DBG: "); Serial.println(A);}


SerialCommand     serialCommand;
boolean           dbg = false;


void setup() {
	Serial.begin(9600);
	serialCommand.addCommand("ledon", SetLedOn);
	serialCommand.addCommand("ledoff", SetLedOff);
	serialCommand.addCommand("temp", GetTemp);
	serialCommand.addCommand("debug", SetDebug);
	serialCommand.setDefaultHandler(UnrecognizedCommand );

	for (byte i = LED_LOW; i <= LED_HIGH; i++) {
		pinMode(i, OUTPUT);
	}

}


void loop() {
	serialCommand.readSerial();
}


void SetLedOn() {
	char *arg = serialCommand.next();

	if (arg != NULL) {
		if (atoi(arg) >= LED_LOW && atoi(arg) <= LED_HIGH) {
			DBGMSG(F("SetLedOn"));
			digitalWrite(atoi(arg), HIGH);
		}
		else {
			DBGMSG(F("out of range"));
		}
	}
	else {
		DBGMSG(F("led not specified"));
	}
}


void SetLedOff() {
	char *arg = serialCommand.next();

	if (arg != NULL) {
		if (atoi(arg) >= LED_LOW && atoi(arg) <= LED_HIGH) {
			DBGMSG(F("SetLedOff"));
			digitalWrite(atoi(arg), LOW);
		}
		else {
			DBGMSG(F("out of range"));
		}
	}
	else {
		DBGMSG(F("led not specified"));
	}
}


void GetTemp() {
	float temperature = -9.99;

	Serial.println(temperature, 2);
}


void SetDebug() {
	char *arg = serialCommand.next();

	if (arg != NULL) {
		if (strcmp(arg, "on") == 0) {
			dbg = true;
			DBGMSG(F("Turn on debug"));
		}
		if (strcmp(arg, "off") == 0) {
			DBGMSG(F("Turn off debug"));
			dbg = false;
		}
	}
}


void UnrecognizedCommand(const char *command) {
	DBGMSG(F("Unrecognized command"));
	DBGMSG(F(" ledon 13  - turn on led connected to digital I/O 13"));
	DBGMSG(F(" ledoff 13 - turn off led connected to digital I/O 13"));
	DBGMSG(F(" temp      - read temperature"));
	DBGMSG(F(" debug on  - turn on debug messages"));
	DBGMSG(F(" debug off - turn off debug messages"));
}

*/